# Boletín GTK (Python)
### 1. CREACIÓN DE VENTANAS
### Ejercicio 1.1
**¿Qué hace la llamada Gtk.main?**

Ejecuta el bucle principal hasta que se llama `gtk_main_quit()`.

**¿Por qué se añade la línea window.connect ("destroy", Gtk.main_quit)?**

Añade la señal que, al ser pulsada, cancela la ejecución del bucle principal

### Ejercicio 1.2
**¿Cuál es la funcionalidad que añade nuestra clase Ventana? ¿Dónde ves aparecer siempre la ventana que se crea?**

La clase representa el concepto de la ventana y en ella se definirán las variables y métodos apropiados para ese objeto, es decir, su comportamiento. Aparece en el centro como se especifica en `Gtk.WindowPosition.CENTER`

### Ejercicio 1.3
**¿Eres capaz de hacer que la ventana se muestre en la posición del ratón? Pega el código de ventana.py tras conseguirlo**
```
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Ventana(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		self.set_position(Gtk.WindowPosition.MOUSE)
```

### 2 COMPONENTE `GTK.BUTON` & CONEXIÓN DE SEÑALES.
### Ejercicio 2.1
**¿Qué significa que la señal "clicked" sea emitida cuando el usuario hace click sobre el botón? ¿Cómo conectamos dicha señal a un método de la clase Ventana para que el método se ejecute al clickar el botón?**

La señal "clicked" indica que el botón ha sido pulsado y que podemos enviar esa señal a un método a través de `button.connect("clicked", self.[método]`

### Ejercicio 2.2
**Conecta el botón al método y luego pega el código de la clase Ventana**
```
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Ventana(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		self.set_position(Gtk.WindowPosition.MOUSE)

		button = Gtk.Button(label="BOTÓN")
		button.connect("clicked", self.ha_clicado)
		self.add(button)

	def ha_clicado(self, widget):
			print("El usuario ha hecho click")
```
**Tras añadir `button.set_size_request(300, 150)` ¿Qué sucede? ¿Puede crecer la ventana más allá del tamaño solicitado por el botón? ¿Puede decrecer?**

La ventana al no tener unas dimensiones por defecto se adaptará al tamaño del botón definido en `button.set_size_request()`, esta nunca será más pequeña del tamaño indicado pero puede ampliarse y el botón se ampliará con ella.

### 3. COMPONENTE `GTK.CHECKBUTTON`
### Ejercicio 3.1
**Añade ahora un `Gtk.CheckButton`. ¿Cuál es la diferencia?**

Ahora en vez de ser un botón pulsable es una checkbox (casilla de verificación)

### Ejercicio 3.2
**Lanza la ventana y marca y desmarca un par de veces el botón. Pega la salida de ejecución.**

```
El checkButton ha sido marcado
El checkButton ha sido desmarcado
El checkButton ha sido marcado
El checkButton ha sido desmarcado
```

### 4. COMPONENTE `GTK.LABEL`
### Ejercicio 4.1
**Haz que la ventana contenga ahora únicamente un label. Pega una captura de pantalla.**

![Captura 4.1](img/captura4.1.png)

### Ejercicio 4.2
**Añadimos un texto mucho más largo. ¿Cómo se ve la ventana? Pega una captura de pantalla.**

![Captura 4.2](img/captura4.2.png)

### Ejercicio 4.3
**Ajustamos ese texto con un line_wrap. ¿Cómo se ve la ventana? Pega una captura de pantalla.**

![Captura 4.3](img/captura4.3.png)

### Ejercicio 4.4
**Vamos a restringir el ancho máximo del label. ¿Cómo se ve la ventana? Pega una captura de pantalla.**

![Captura 4.4](img/captura4.4.png)

**¿Qué pasa si agrandas la ventana? ¿El texto se adapta al ancho de la ventana?**
 Si, el texto se adapta al ancho de la ventana

### Ejercicio 4.5
**A continuación modifica la justificación del label. ¿Qué es lo que sucede? Pega un pantallazo**

![Captura 4.5](img/captura4.5.png)

**¿Cuál es la justificación por defecto del texto si no se usa la línea `set_justify()`?**
 Si no se pone nada se justifica a la izquierda

### 5. EMPACAR MÁS DE UN COMPONENTE
### Ejercicio 5.1
**En vez de `self.add()` usaremos `box.pack_start()`. Agranda la ventana y pega un pantallazo.**

![Captura 5.1](img/captura5.1.png)

**¿Qué es lo que pasa?**
 El texto no se adapta al tamaño de la ventana, se queda en la izquierda

### Ejercicio 5.2
**Cambiándolo por `box.pack_end()`... Agranda la ventana y sube un pantallazo de lo que veas.**

![Captura 5.2](img/captura5.2.png)


### Ejercicio 5.3
**Empaqueta un botón como segundo componente. ¿Qué sucede cuando agrandas la ventana horizontalmente? Sube una captura**
EL botón se adapta a la pantalla y nunca será más pequeño del tamaño especificado.

![Captura 5.3](img/captura5.3.png)

**¿Por qué el label no crece más allá de 50 caracteres de ancho?**
El texto no pasa de 50 caracteres debido a `label.set_max_width_chars(50)`.

**¿Se reparte el espacio equitativamente entre ambos componentes cuando agrandas lo suficiente?**
Las propiedades de PackStart son las siguientes: `pack_start(child, expand, fill, padding)`. Al estar todas en true los widgets se expanden y rellenan la ventana, por lo que se reparte el espacio equitativamente


### Ejercicio 5.4
**Sube pantallazo de cómo se ve la pantalla si añades esta línea `box.pack_start(label, False, False, 0)`**

![Captura 5.4](img/captura5.4.png)

### Ejercicio 5.5
**Pon la propiedad Fill y Expand a False y sube un pantallazo.**

![Captura 5.5](img/captura5.5.png)

**¿Cómo se comporta la ventana cuando la agrandas?**
Al cambiar las propiedades a False, los widgets de la ventana no se expanden ni la rellenan

### Ejercicio 5.6
**Utiliza `box.pack_end()` en Button. ¿Qué pasa ahora en la ventana al agrandarla? Sube un pantallazo.**

![Captura 5.6](img/captura5.6.png)

### 6. `GTK.IMAGE`
### Ejercicio 6.1
**El componente Gtk.Image nos permite añadir imágenes a nuestras interfaces.Sube un pantallazo de cómo se ve**

![Captura 6.1](img/captura6.1.png)

### Ejercicio 6.2
**Reemplaza las sentencias con `fill=True`. ¿Cómo se ve la ventana al agrandarla? Sube un pantallazo.**

![Captura 6.2](img/captura6.2.png)

### Ejercicio 6.3
**Forzar a la ventana a que tenga un tamaño fijo más pequeño en el que no caben las imágenes en alto. Sube un pantallazo**

![Captura 6.3](img/captura6.3.png)

### 7. CREACIÓN DE UN COMPONENTE CON `GTK.BUTTON` Y `GTK.LABEL`. ACERTIJO.
### Ejercicio 7.1
**Crea un nuevo fichero componente_adivinanza.py y modifica ventana.py con el código indicado. Sube un pantallazo**

![Captura 7.1](img/captura7.1.png)
