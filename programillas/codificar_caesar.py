abecedario = "abcdefghijklmnñopqrstuvwxyz"
mensaje = input("Escribe tu mensaje a cifrar: ")
mensaje_cifrado = ""
for letra in mensaje:
    nueva_posicion = abecedario.find(letra) + 1
    if nueva_posicion >= len(abecedario): nueva_posicion = 0
    if letra in abecedario:
        mensaje_cifrado += abecedario[nueva_posicion]
    else:
        mensaje_cifrado += letra
print(mensaje)
print(mensaje_cifrado)
