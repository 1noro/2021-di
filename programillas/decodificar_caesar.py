abecedario = "abcdefghijklmnñopqrstuvwxyz"
mensaje_cifrado = input("Escribe tu mensaje cifrado: ")
mensaje = ""
for letra in mensaje_cifrado:
    nueva_posicion = abecedario.find(letra) - 1
    if nueva_posicion < 0: nueva_posicion = len(abecedario) - 1
    if letra in abecedario:
        mensaje += abecedario[nueva_posicion]
    else:
        mensaje += letra
print(mensaje_cifrado)
print(mensaje)
