# Introducción a los comandos GIT

> 20200930

Secuencia de comandos utilizados en clase para probar que funciona el GIT

> Tener en cuenta que yo tengo configurado GIT por SSH.

### Configurar usuario e e-mail

```
git config --global user.name "nombre-de-usuario"
git config --global user.email "email@example.com"
```

### Clonar un repositorio y crear un nuevo archivo

```
pwd #Para saber en que directorio estamos
git clone git@gitlab.afundacionfp.com:alfredo.rodriguez/desarrollo-de-interfaces.git
cd desarrollo-de-interfaces
echo "Hola mundo." > README.md
git add README.md #Yo prefiero poner "git add ."
git commit -m "Creado README.md"
git push -u origin master
# Y como lo tengo configurado por SSH no me pode usuario ni contraseña
```

### Descargar cambios desde un repositorio (pull)

```
pwd #Para saber en que directorio estamos
cd desarrollo-de-interfaces
git pull
# Y como lo tengo configurado por SSH no me pode usuario ni contraseña
```
